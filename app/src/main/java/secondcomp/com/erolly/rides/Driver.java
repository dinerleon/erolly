package secondcomp.com.erolly.rides;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import secondcomp.com.erolly.ui.user.User;

/**
 * Created by Regina on 16/03/2018.
 */

public class Driver implements Serializable{
    private int userId;
    private String name;
    private String address;
    private User.Status userStatus;
    private String phoneNum;
    private String image;
    private Date date;

    private List<User> drivers;

    private List<User> riders;

    public Driver (){}

    public Driver(String name, String address, String message, String image, int userId, List<User.Status> userStatus, List<User> riders, String phoneNum) {
        this.name = name;
        this.phoneNum = phoneNum;
        this.address = address;
        this.userStatus = User.Status.USER;
        this.userId = userId;
    }

    public int getId() {
        return userId;
    }

    public void setId(int id) {
        this.userId = id;
    }

    public String getTitle() {
        return name;
    }

    public void setTitle(String title) {
        this.name = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMessage() {
        return phoneNum;
    }

    public void setMessage(String message) {
        this.phoneNum = message;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<User> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<User> drivers) {
        this.drivers = drivers;
    }

    public List<User> getRiders() {
        return riders;
    }

    public void setRiders(List<User> riders) {
        this.riders = riders;
    }
}
