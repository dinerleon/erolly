package secondcomp.com.erolly.rides;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import secondcomp.com.erolly.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DriversFragment extends Fragment {
private RecyclerView driversRv;

    public DriversFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_drivers, container, false);
        driversRv =  v.findViewById(R.id.driversList);
        driversRv.setLayoutManager(new LinearLayoutManager(getContext()));
        return v;
    }

}
