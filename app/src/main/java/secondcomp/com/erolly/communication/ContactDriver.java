package secondcomp.com.erolly.communication;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import secondcomp.com.erolly.R;
import secondcomp.com.erolly.events.Event;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactDriver extends Fragment implements View.OnClickListener{
    private static final int CALL_PHONE_PERMISSION = 89;
    private Event event;


    public ContactDriver() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact_driver, container, false);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case (R.id.callDriver):
                onCall();
                break;
//            case (R.id.btnMap):
//                Bundle bundle = new Bundle();
//                bundle.putParcelable("selectedClinic", clinic);
//                Intent goToMap = new Intent(this, MapsActivity.class);
//                goToMap.putExtras(bundle);
//                startActivity(goToMap);
//                break;
//            case (R.id.btnNavigate):
////                String uri = "geo:" + event.get + "," + clinic.getLon() + "?q="
////                        + clinic.getClinicAddress();
////                Intent navigate = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
////                navigate.setData(Uri.parse(uri));
//                PackageManager packageManager = getActivity().getPackageManager();
//                List activities = packageManager.queryIntentActivities(navigate,
//                        PackageManager.MATCH_DEFAULT_ONLY);
//                String title = "How would you like to navigate there?";
//                // Create intent to show chooser
//                Intent chooser = Intent.createChooser(navigate, title);
//                boolean isIntentSafe = activities.size() > 0;
//                // Verify the intent will resolve to at least one activity
//                if (navigate.resolveActivity(getActivity().getPackageManager()) != null) {
//                    if (isIntentSafe) {
//                        startActivity(chooser);
//                    }
//                }
//                break;
        }
    }

    public void onCall(){
        int permissionCheck = ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.CALL_PHONE);
        String phoneNumber = event.getPhone();
        Intent callNumber = new Intent(Intent.ACTION_CALL, Uri.fromParts("tel", phoneNumber, null));
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    getActivity(),
                    new String[]{Manifest.permission.CALL_PHONE},
                    CALL_PHONE_PERMISSION);
        } else {
            startActivity(callNumber);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case CALL_PHONE_PERMISSION:
                if ((grantResults.length > 0) && (grantResults[0]
                        == PackageManager.PERMISSION_GRANTED)) {
                    onCall();
                } else {
                    Log.d("TAG", "Call Permission Not Granted");
                }
                break;
            default:
                break;
        }
    }
}
