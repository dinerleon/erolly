package secondcomp.com.erolly.events;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import secondcomp.com.erolly.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventDetailActivity extends AppCompatActivity implements View.OnClickListener{
private ImageView eventDetailImage;
   private TextView eventNameDetail,
           eventAddressDetail,
           eventTimeDetail,
           hostPhoneDetail,
           messageDetail;
   private Button needAride, iWillDrive;

    public EventDetailActivity() {
        // Required empty public constructor
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_event_detail);

        eventDetailImage = findViewById(R.id.eventDetailImage);
        eventNameDetail = findViewById(R.id.eventNameDetail);
        eventAddressDetail = findViewById(R.id.eventAddressDetail);
        eventTimeDetail = findViewById(R.id.eventTimeDetail);
        hostPhoneDetail = findViewById(R.id.hostPhoneDetail);
        messageDetail = findViewById(R.id.messageDetail);
        needAride = findViewById(R.id.needAride);
        iWillDrive = findViewById(R.id.iWillDrive);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case (R.id.needAride):

                break;

            case (R.id.iWillDrive):

                break;
        }
    }
}
