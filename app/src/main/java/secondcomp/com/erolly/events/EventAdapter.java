package secondcomp.com.erolly.events;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import secondcomp.com.erolly.R;

/**
 * Created by Regina on 16/03/2018.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventHolder> {
    private List<Event> events = new ArrayList<>();
    private Context context;

    public EventAdapter(Context context, List<Event> events) {
        this.context = context;
        this.events = events;
    }

    @Override
    public EventHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_event_item, parent, false);
        return new EventHolder(view);
    }

    @Override
    public void onBindViewHolder(EventAdapter.EventHolder holder, int position) {
        //holder.bind(events.get(position));

        Event event = events.get(position);
        if (event.getImage() == null || event.getImage().equals("") || event.getImage().equals("some url")) {
            event.setImage("https://images.pexels.com/photos/566454/pexels-photo-566454.jpeg?h=350&dpr=2&auto=compress&cs=tinysrgb.jpg");
        }

        Glide.with(context).load(event.getImage()).into(holder.eventImage);
        holder.eventTitle.setText(event.getTitle());
        holder.eventTitle.setText(event.getTitle());
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public class EventHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView eventTitle, eventAddress, clinicDistance;
        private ImageView eventImage;
        private Event event;
        private String imagePath;
        private String forDistance;

        public EventHolder(View clinicView) {
            super(clinicView);
            eventImage = (ImageView) clinicView.findViewById(R.id.eventImage);
            eventTitle = (TextView) clinicView.findViewById(R.id.eventName);
            eventAddress = (TextView) clinicView.findViewById(R.id.address);
            //clinicDistance = (TextView) clinicView.findViewById(R.id.clinicDistance);
            clinicView.setOnClickListener(this);
        }

        public void bind(Event event) {
            this.event = event;
            //clinicPrefs = PreferenceManager.getDefaultSharedPreferences(context);

            //double distance = clinic.getDistance();
            DecimalFormat formater = new DecimalFormat("0.0000000");
            eventTitle.setText(event.getTitle());
            eventAddress.setText(event.getAddress());
//            if (distance > 1) {
//                clinicDistance.setText(String.valueOf(formater.format(distance)) + " Km. from you");
//                forDistance = String.valueOf(formater.format(distance)) + " Km. from you";
//            } else {
//                clinicDistance.setText(String.valueOf(formater.format(distance * 1000)) + " m. from you");
//                forDistance = String.valueOf(formater.format(distance)) + " m. from you";
//            }
//            clinicPrefs.edit().putString("distance", forDistance).commit();
//            imagePath = BASE_URL + clinic.getClinicImagePath() + API_KEY;
//            Picasso.with(context).load(imagePath).into((clinicImage));
            imagePath = event.getImage();
            Glide.with(context).load(imagePath).into(eventImage);
        }

        @Override
        public void onClick(View v) {
            switch (getAdapterPosition()) {
                default:
                    //clinicPrefs = PreferenceManager.getDefaultSharedPreferences(context);
//                    clinicPrefs.edit().putString("distanceSelected",String.valueOf(clinicDistance.getText())).commit();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("event", event);
                    Intent detailView = new Intent(context, EventDetailActivity.class);
                    detailView.putExtras(bundle);
                    context.startActivity(detailView);
                    break;
            }
        }
    }

    public void updateData(List<Event> e) {
        events = e;
    }

}
