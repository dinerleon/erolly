package secondcomp.com.erolly.events;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import secondcomp.com.erolly.ui.user.User;

/**
 * Created by Aharon on 16/03/2018.
 */

public class Event implements Serializable {

    private int id;
    private String title;
    private String address;
    private String message;
    private String phone;
    private String image;
    private Date date;

    private List<User> drivers;

    private List<User> riders;

    public Event (){}

    public Event(String name, String address, String message, String image, Date date, List<User> drivers, List<User> riders, String phone) {
        this.title = name;
        this.phone = phone;
        this.riders = riders;
        this.drivers = drivers;
        this.address = address;
        this.message = message;
        this.image = image;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<User> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<User> drivers) {
        this.drivers = drivers;
    }

    public List<User> getRiders() {
        return riders;
    }

    public void setRiders(List<User> riders) {
        this.riders = riders;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
