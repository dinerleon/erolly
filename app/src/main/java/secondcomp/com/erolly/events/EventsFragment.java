package secondcomp.com.erolly.events;


import android.content.Intent;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import secondcomp.com.erolly.R;
import secondcomp.com.erolly.ui.event.AddEventActivity;
import secondcomp.com.erolly.data.network.GetEventsAsyncTask;
import secondcomp.com.erolly.data.network.GetEventsServerCallback;


/**
 * A simple {@link Fragment} subclass.
 */
public class EventsFragment extends Fragment implements SearchView.OnQueryTextListener, View.OnClickListener, GetEventsServerCallback, SwipeRefreshLayout.OnRefreshListener {
    private SearchView searchView;
    private List<Event> eventsList = new ArrayList<>();
    private EventAdapter adapter;
    private RecyclerView recyclerView;
    private FloatingActionButton fab;
    private SwipeRefreshLayout swipeView;

    public static EventsFragment getNewInstance() {
        EventsFragment fragment = new EventsFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);

        return fragment;
    }

    public EventsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View v = inflater.inflate(R.layout.fragment_events_list, container, false);
        recyclerView = v.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        fab = v.findViewById(R.id.createEvent);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AddEventActivity.class);
                startActivity(intent);
            }
        });
        searchView = v.findViewById(R.id.search_main);
        searchView.setOnQueryTextListener(this);

        swipeView = (SwipeRefreshLayout) v.findViewById(R.id.swiperefresh);
        swipeView.setOnRefreshListener(this);

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeView.setRefreshing(true);
        new GetEventsAsyncTask(this).execute();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (query.length() >= 1) {
            filterTopics(query);
        } else {
            filterTopics("");

        }
        return true;
    }

    public List<Event> filterTopics(String searchWord) {
        List<Event> filteredList = new ArrayList<>();
        if (searchWord.equalsIgnoreCase("")) {
            filteredList = this.eventsList;
        } else {
            Event currentEvent;
            String currentEventTitle;
            for (int i = 0; i < this.eventsList.size(); i++) {
                currentEvent = this.eventsList.get(i);
                currentEventTitle = currentEvent.getTitle();
                if (currentEventTitle.toLowerCase().contains(searchWord.toLowerCase())) {
                    filteredList.add(currentEvent);

                }
            }
        }
        bindTopicsToRecyclerView(filteredList);
        return filteredList;
    }

    public void bindTopicsToRecyclerView(List<Event> filteredTopics) {
        adapter = new EventAdapter(getContext(), filteredTopics);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (newText.length() >= 1) {
            filterTopics(newText);
        } else {
            filterTopics("");
        }
        return true;
    }


    @Override
    public void onClick(View view) {

    }


    @Override
    public void onGetEventsSuccess(List<Event> events) {
        Log.d("ErollyTag", "" + events.size());
        swipeView.setRefreshing(false);
        eventsList = events;
        bindTopicsToRecyclerView(events);
    }

    @Override
    public void onGetEventsError() {
        swipeView.setRefreshing(false);
        Toast.makeText(getContext(), "Server Error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRefresh() {
        new GetEventsAsyncTask(this).execute();
    }
}
