package secondcomp.com.erolly.data.network;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import secondcomp.com.erolly.ui.user.User;

/**
 * Created by Aharon on 16/03/2018.
 */

public class RegisterToServer {

    private User user;

    public RegisterToServer(User user){
        this.user = user;
    }

    public void sendUser() {
        new AsyncTask<User, Void, String>() {
            @Override
            protected String doInBackground(User... users) {
                HttpURLConnection urlConnection = null;
                InputStream inputStream = null;
                OutputStream outputStream = null;
                URL url = null;
                String result="";
                try {
                    url = new URL("http://35.224.176.105/eRolly/user/");
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setUseCaches(false);
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                    urlConnection.setDoOutput(true);
                    urlConnection.connect();
                    JSONObject jsonObject = new JSONObject();
                    if (user.getId() != null){
                        jsonObject.put("sits", users[0].getSits());
                        jsonObject.put("message", users[0].getMessage());
                    }
                    jsonObject.put("userId", users[0].getUserId());
                    jsonObject.put("name", users[0].getName());
                    jsonObject.put("userStatus", users[0].getUserStatus());
                    jsonObject.put("address", users[0].getAddress());
                    jsonObject.put("phoneNum", users[0].getPhoneNum());
                    jsonObject.put("lon", users[0].getLon());
                    jsonObject.put("lat", users[0].getLat());
                    outputStream = urlConnection.getOutputStream();
                    outputStream.write(jsonObject.toString().getBytes());
                    outputStream.close();
                    inputStream = urlConnection.getInputStream();
                    byte [] buffer = new byte[128];
                    int a = inputStream.read(buffer);
                    result = new String(buffer,0,a);
                    Log.d("TAG",result);
                    inputStream.close();
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (ProtocolException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return result;
            }
        }.execute(user);
    }
}
