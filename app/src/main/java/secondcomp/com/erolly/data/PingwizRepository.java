package secondcomp.com.erolly.data;

import android.arch.lifecycle.LiveData;
import android.net.Uri;

import com.google.firebase.auth.FirebaseUser;

import secondcomp.com.erolly.AppExecutors;
import secondcomp.com.erolly.data.network.PingwizNetworkDataSource;

public class PingwizRepository {

    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static PingwizRepository sInstance;
    private final PingwizNetworkDataSource mPingwizNetworkDataSource;
    private final AppExecutors mExecutors;

    private PingwizRepository(PingwizNetworkDataSource pingwizNetworkDataSource,
                              AppExecutors executors) {
        mPingwizNetworkDataSource = pingwizNetworkDataSource;
        mExecutors = executors;
    }

    public synchronized static PingwizRepository getInstance(
            PingwizNetworkDataSource pingwizNetworkDataSource,
            AppExecutors executors) {
        if (sInstance == null) {
            synchronized (LOCK) {
                sInstance = new PingwizRepository(pingwizNetworkDataSource,
                        executors);
            }
        }
        return sInstance;
    }

    public void setFirebaseUser(FirebaseUser firebaseUser) {
        mPingwizNetworkDataSource.setFirebaseUser(firebaseUser);
    }

    public LiveData<Uri> getUserPhotoUri() {
        return mPingwizNetworkDataSource.getUserPhotoUri();
    }

    public LiveData<String> getUserName() {
        return mPingwizNetworkDataSource.getUserName();
    }

    public LiveData<String> getUserEmail() {
        return mPingwizNetworkDataSource.getUserEmail();
    }

}
