package secondcomp.com.erolly.data.network;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import secondcomp.com.erolly.events.Event;

/**
 * Created by shirlyk on 3/16/18.
 */

public class GetEventsAsyncTask extends AsyncTask<Void, Void, List<Event>> {

    private GetEventsServerCallback mServerCallback;

    public GetEventsAsyncTask(GetEventsServerCallback serverCallback) {
        mServerCallback = serverCallback;
    }

    @Override
    protected List<Event> doInBackground(Void... voids) {
        List<Event> events = new ArrayList<>();
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        OutputStream outputStream = null;
        URL url = null;
        String result = "";
        try {
            url = new URL("http://35.224.176.105/eRolly/events/");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setUseCaches(false);
            urlConnection.connect();
            inputStream = urlConnection.getInputStream();
            int actuallyRead;
            StringBuilder stringBuilder = new StringBuilder();
            byte[] buffer = new byte[256];
            while ((actuallyRead = inputStream.read(buffer)) != -1) {
                stringBuilder.append(new String(buffer, 0, actuallyRead));
            }
            inputStream.close();
            JSONArray jsonArray = new JSONArray(stringBuilder.toString());
            for (int i = 0; i < jsonArray.length(); i++) {
                Event event = new Event();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                event.setTitle(jsonObject.getString("title"));
                event.setAddress(jsonObject.getString("address"));
                event.setId(jsonObject.getInt("id"));
                event.setImage(jsonObject.getString("image"));
                event.setMessage(jsonObject.getString("message"));
                event.setPhone(jsonObject.getString("phone"));
                event.setDate(Calendar.getInstance().getTime()); // TODO fix this
                events.add(event);

            }
        } catch (Exception e) {
            Log.d("ErollyTag", e.getMessage());
        }
        return events;
    }

    @Override
    protected void onPostExecute(List<Event> events) {
        super.onPostExecute(events);
        mServerCallback.onGetEventsSuccess(events);
    }
}
