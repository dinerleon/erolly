package secondcomp.com.erolly.data.network;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import secondcomp.com.erolly.events.Event;

/**
 * Created by shirlyk on 3/16/18.
 */

public class SendEventAsyncTask extends AsyncTask<Event, Void, String> {

    @Override
    protected String doInBackground(Event... events) {
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        OutputStream outputStream = null;
        URL url = null;
        String result = "";
        try {
            url = new URL("http://35.224.176.105/eRolly/event/");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setUseCaches(false);
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("title", events[0].getTitle());
            jsonObject.put("image", events[0].getImage());
            jsonObject.put("address", events[0].getAddress());
            jsonObject.put("phone", events[0].getPhone());
            jsonObject.put("date", "2018-03-15T18:13:25.199+0000"); // TODO
            jsonObject.put("message", events[0].getMessage());
            outputStream = urlConnection.getOutputStream();
            outputStream.write(jsonObject.toString().getBytes());
            outputStream.close();
            inputStream = urlConnection.getInputStream();
            byte[] buffer = new byte[128];
            int a = inputStream.read(buffer);
            result = new String(buffer, 0, a);
            Log.d("TAG", result);
            inputStream.close();
        } catch (Exception e) {
            Log.e("ErollyTag", e.getMessage());
        }

        return result;
    }

}
