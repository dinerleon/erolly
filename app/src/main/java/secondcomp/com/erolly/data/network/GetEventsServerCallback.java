package secondcomp.com.erolly.data.network;

import java.util.List;

import secondcomp.com.erolly.events.Event;

/**
 * Created by shirlyk on 3/16/18.
 */

public interface GetEventsServerCallback {

    void onGetEventsSuccess(List<Event> events);

    void onGetEventsError();
}
