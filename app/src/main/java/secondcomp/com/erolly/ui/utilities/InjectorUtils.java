package secondcomp.com.erolly.ui.utilities;

import android.content.Context;

import secondcomp.com.erolly.AppExecutors;
import secondcomp.com.erolly.data.PingwizRepository;
import secondcomp.com.erolly.data.network.PingwizNetworkDataSource;
import secondcomp.com.erolly.ui.user.UserInfoViewModelFactory;

public class InjectorUtils {

    public static PingwizRepository provideRepository(Context context) {
        AppExecutors executors = AppExecutors.getInstance();
        PingwizNetworkDataSource networkDataSource =
                PingwizNetworkDataSource.getInstance(context.getApplicationContext(), executors);
        return PingwizRepository.getInstance(networkDataSource, executors);
    }

    public static PingwizNetworkDataSource provideNetworkDataSource(Context context) {
        AppExecutors executors = AppExecutors.getInstance();
        return PingwizNetworkDataSource.getInstance(context.getApplicationContext(), executors);
    }

    public static UserInfoViewModelFactory provideUserInfoViewModelFactory(Context context) {
        PingwizRepository repository = provideRepository(context.getApplicationContext());
        return new UserInfoViewModelFactory(repository);
    }
}
