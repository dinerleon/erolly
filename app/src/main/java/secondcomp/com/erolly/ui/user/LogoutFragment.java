package secondcomp.com.erolly.ui.user;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import secondcomp.com.erolly.ui.utilities.InjectorUtils;

public class LogoutFragment extends Fragment {

    private UserInfoViewModel mViewModel;

    public static LogoutFragment getNewInstance() {
        LogoutFragment fragment = new LogoutFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Activity activity = getActivity();
        if (activity == null) return;
        Context applicationContext = activity.getApplicationContext();

        // Get the ViewModel from the factory
        UserInfoViewModelFactory factory = InjectorUtils.provideUserInfoViewModelFactory(applicationContext);
        mViewModel = ViewModelProviders.of(this, factory).get(UserInfoViewModel.class);

        performLogout();
    }

    public void performLogout() {
        Context context = getContext();
        if (context != null) {
            Context applicationContext = getContext().getApplicationContext();
            AuthUI.getInstance()
                    .signOut(applicationContext)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        public void onComplete(@NonNull Task<Void> task) {
                            mViewModel.setFirebaseUser(null);
                        }
                    });
        }
    }
}
