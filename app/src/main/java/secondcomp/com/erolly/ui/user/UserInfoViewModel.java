package secondcomp.com.erolly.ui.user;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.ViewModel;
import android.net.Uri;

import com.google.firebase.auth.FirebaseUser;

import secondcomp.com.erolly.data.PingwizRepository;

public class UserInfoViewModel extends ViewModel {

    private final LiveData<Uri> mUserPhotoUri;
    private final LiveData<String> mUserName;
    private final LiveData<String> mUserEmail;

    private final PingwizRepository mRepository;

    public UserInfoViewModel(PingwizRepository repository) {
        mRepository = repository;
        mUserPhotoUri = mRepository.getUserPhotoUri();
        mUserName = mRepository.getUserName();
        mUserEmail = mRepository.getUserEmail();
    }

    public void setFirebaseUser(FirebaseUser firebaseUser) {
        mRepository.setFirebaseUser(firebaseUser);
    }

    public LiveData<Uri> getUserPhotoUri() {
        return mUserPhotoUri;
    }

    public LiveData<String> getUserName() {
        return mUserName;
    }

    public LiveData<String> getUserEmail() {
        return mUserEmail;
    }
}
