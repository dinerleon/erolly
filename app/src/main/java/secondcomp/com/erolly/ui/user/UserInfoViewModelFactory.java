package secondcomp.com.erolly.ui.user;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import secondcomp.com.erolly.data.PingwizRepository;

/**
 * Factory method that allows us to create a ViewModel with a constructor that takes a
 */
public class UserInfoViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    private final PingwizRepository mRepository;

    public UserInfoViewModelFactory(PingwizRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new UserInfoViewModel(mRepository);
    }
}
